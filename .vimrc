syntax on
set conceallevel=2
set mouse=a
set hlsearch
set autoread
set ignorecase
set number
set showmatch
set cursorline
set laststatus=2
set errorbells
set visualbell
set ruler
set linebreak
syntax enable
set ruler
set virtualedit=onemore
set list
filetype plugin indent on
set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
" By default timeoutlen is 1000 ms
set timeoutlen=500

" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
"set expandtab
syntax match Tab /\t/
hi Tab gui=underline

" highlight on yank
au TextYankPost * lua vim.highlight.on_yank {higroup="IncSearch", timeout=150, on_visual=true}

" KEY MAPPINGS

" leader key
let mapleaderkey = ","
" install vim-plug if it isn't installed yet
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
      \| PlugInstall --sync | source $MYVIMRC
      \| endif

call plug#begin('~/.vim/plugged')

"Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

"dracula theme
Plug 'Mofiqul/dracula.nvim'
let g:dracula_transparent_bg = 'true'
let g:termguicolors = 'true'


" python notebooks
Plug 'jupyter-vim/jupyter-vim'

"python format
Plug 'averms/black-nvim', {'do': ':UpdateRemotePlugins'}

"Language tool
Plug 'vigoux/LanguageTool.nvim'

" Conquer Of Completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
vmap <leader>a <Plug>(coc-codeaction-selected)
highlight CocFloating ctermbg=0 guibg=#44475a
command! -nargs=0 Prettier :CocCommand prettier.formatFile "prettier
"Polyglot Language Packs
Plug 'sheerun/vim-polyglot'

" ale
Plug 'dense-analysis/ale'
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   'javascript': ['prettier'],
\   'css': ['prettier'],
\   'html': ['prettier'],
\   'markdown': ['prettier'],
\}

" arduino
Plug 'stevearc/vim-arduino'

" Autopairs
Plug 'jiangmiao/auto-pairs'

" Lualine
Plug 'nvim-lualine/lualine.nvim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kdheepak/tabline.nvim'

" Vim-which-key
Plug 'https://github.com/liuchengxu/vim-which-key.git'

" Start screen
Plug 'mhinz/vim-startify'

" Graphviz
Plug 'liuchengxu/graphviz.vim'

"Spelunker
Plug 'kamykn/spelunker.vim'
let g:enable_spelunker_vim = 0

"popup menu
Plug 'kamykn/popup-menu.nvim'

"Tex Conceal
Plug   'KeitaNakamura/tex-conceal.vim', {'for': 'tex'}
let g:tex_flavor = 'latex'
let g:tex_conceal='abdmg'
let g:tex_conceal_frac=1

"Vim markdown
"Plug 'plasticboy/vim-markdown'
"let g:vim_markdown_toc_autofit = 0
"let g:vim_markdown_conceal = 1
"let g:markdown_folding = 0

"Initialize Plugin System
call plug#end()

" activate colorscheme
colorscheme dracula

"lua settings

" lualine"
lua << END
require('lualine').setup({
  options = {
    theme = 'dracula',
    component_separators = {left = '', right = ''},
    section_separators = {left = '', right = ''},},

  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff',{'diagnostics', sources={'nvim_diagnostic', 'coc'}}},
    lualine_c = {'filename'},
    lualine_x = {'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  }
})
END

lua << END
require('tabline').setup()
END
